import { ApplicationService } from '@themost/common';
import {
	SchemaLoaderStrategy,
	DataConfigurationStrategy,
	ODataModelBuilder,
	DataContext,
	DataModel,
} from '@themost/data';
import path from 'path';
import fs from 'fs';
import pluralize from 'pluralize';
import { cloneDeep } from 'lodash';

class LocalDataContext extends DataContext {
	constructor(configuration) {
		super();
		this.getConfiguration = function () {
			return configuration;
		};
	}

	model(name) {
		const strategy = this.getConfiguration().getStrategy(
			DataConfigurationStrategy
		);
		if (strategy.dataTypes.hasOwnProperty(name)) {
			return;
		}
		const definition = strategy.model(name);
		if (definition == null) {
			return;
		}
		const res = new DataModel(definition);
		res.context = this;
		return res;
	}
}

export class VersioningService extends ApplicationService {
	PREVIOUS_STATE_LISTENER = '@themost/data/previous-state-listener';
	// TODO: Maybe detach this listener from the parent project at a later stage
	DATA_LOCALIZATION_LISTENER = './listeners/data-localization';

	constructor(app) {
		super(app);
		this.install(app);
	}

	install(app) {
		const schemaLoader = app
			.getConfiguration()
			.getStrategy(SchemaLoaderStrategy);
		const dataConfiguration = app
			.getConfiguration()
			.getStrategy(DataConfigurationStrategy);
		// get all model names
		const names = schemaLoader.getModels();
		const cleanup = [];
		names
			// filter revision models that have the versioning
			// attribute set to true
			.filter((name) => {
				const model = dataConfiguration.getModelDefinition(name);
				return model.versioning && model.versioning.disabled!==true;
			})
			.forEach((name) => {
				// create revision
				const model = this.createDataModelRevision(name);
				cleanup.push(model);
			});
		/**
		 * get model builder
		 * @type {ODataModelBuilder}
		 */
		const builder = this.getApplication().getStrategy(ODataModelBuilder);
		if (builder) {
			cleanup.forEach((model) => {
				const entitySet = pluralize.plural(model.name);
				builder.addEntitySet(model.name, entitySet);
				// check if model is hidden
				if (model.hidden) {
					// and ignore it
					builder.ignore(model.name);
				}
				// refresh model entity set
				(function refreshEntitySet(entityType, entitySet) {
					builder.removeEntitySet(entitySet);
					builder.addEntitySet(entityType, entitySet);
				})(model.name, entitySet);
			});
		}
	}

	uninstall(app) {
		//
	}

	createDataModelRevision(name) {
		const dataConfiguration = this.getApplication()
			.getConfiguration()
			.getStrategy(DataConfigurationStrategy);
		// get model
		const model = dataConfiguration.getModelDefinition(name);
		const revisionModel = {
			name: `${name}Revision`, // e.g StudentRevision
			implements: name, // e.g Student
			hidden: true,
			version: model.version,
			sealed: false,
			fields: [
				{
					name: 'id',
					type: 'Guid',
					nullable: false,
					primary: true,
					value: 'javascript:return this.newGuid();',
				},
				{
					name: 'revision_object',
					type: name,
					nullable: false,
				},
				{
					name: 'revision_version',
					type: 'Text',
					size: 24,
					nullable: false,
				},
				{
					name: 'revision_tag',
					type: 'Text',
					size: 24,
					nullable: true,
				},
				{
					name: 'revision_date',
					type: 'DateTime',
					readonly: true,
					nullable: false,
					value: 'javascript:return (new Date());'
				},
				{
					name: 'revision_creator',
					type: 'User',
					nullable: true,
					readonly: true,
					value: 'javascript:return this.user();',
				},
			],
			// preserve read privileges of parent model
			privileges: cloneDeep(model.privileges)
				.filter((privilege) => {
					if ((privilege.mask & 1) === 1) {
						return true;
					}
					return false;
				})
				.map((privilege) => {
					return Object.assign(privilege, { mask: 1 });
				}),
		};
		model.fields = model.fields || [];
		model.fields.forEach((field) => {
			// if the field is readonly, delete the attribute and push it to revision model
			// important note: do not include id, locale and locales,
			// which are fields that interfere with this process
			// do not check for field.primary
			if (
				field.readonly &&
				field.name !== 'id' &&
				field.name !== 'locales' &&
				field.name !== 'locale'
			) {
				// clone field to prevent change in parent model
				const removeReadonly = cloneDeep(field);
				// delete readonly attribute
				delete removeReadonly.readonly;
				// and push to revision model
				revisionModel.fields.push(removeReadonly);
			}
			// assigned by schema loader
			delete field.model;
		});
		model.fields
			.filter((field) => field.mapping)
			.forEach((field) => {
				// adapt field mappings
				const cloneField = cloneDeep(field);
				// find primary key (create a local context)
				const context = new LocalDataContext(
					this.getApplication().getConfiguration()
				);
				const parentModelPrimaryKey =
					context.model(model.name).primaryKey || 'id';
				// todo: handle clustered primary keys at a later stage
				if (cloneField.mapping.parentModel === model.name) {
					cloneField.mapping.parentModel = revisionModel.name;
					// adapt id mapping (convert to revision_object)
					if (cloneField.mapping.parentField === parentModelPrimaryKey) {
						cloneField.mapping.parentField = 'revision_object';
					}
				}
				if (cloneField.mapping.childModel === model.name) {
					cloneField.mapping.childModel = revisionModel.name;
					// adapt id mapping (convert to revision_object)
					if (cloneField.mapping.childField === parentModelPrimaryKey) {
						cloneField.mapping.childField = 'revision_object';
					}
				}
				revisionModel.fields.push(cloneField);
			});
		// if the parent model has the locales attribute
		const locales = model.fields.find((field) => field.name === 'locales');
		if (locales) {
			this.createLocaleModel(revisionModel, model);
			// rebuild locales and locale property
			const localesIndex = revisionModel.fields.findIndex(
				(field) => field.name === 'locales'
			);
			revisionModel.fields.splice(localesIndex, 1);
			const localeIndex = revisionModel.fields.findIndex(
				(field) => field.name === 'locale'
			);
			if (localeIndex) {
				revisionModel.fields.splice(localeIndex, 1);
			}
			revisionModel.fields.push({
				name: 'locales',
				type: `${revisionModel.name}Locale`,
				mapping: {
					associationType: 'association',
					cascade: 'delete',
					parentModel: revisionModel.name,
					parentField: 'id',
					childModel: `${revisionModel.name}Locale`,
					childField: 'object',
				},
			});
			revisionModel.fields.push({
				name: 'locale',
				type: `${revisionModel.name}Locale`,
				readonly: true,
				editable: false,
				many: true,
				expandable: true,
				multiplicity: 'ZeroOrOne',
				mapping: {
					associationType: 'association',
					cascade: 'delete',
					parentModel: revisionModel.name,
					parentField: 'id',
					childModel: `${revisionModel.name}Locale`,
					childField: 'object',
					options: {
						$filter: 'inLanguage eq lang()',
						$first: true,
					},
				},
			});
		}
		// find nested parent model attributes
		model.fields
			.filter((field) => field.nested)
			.forEach((nestedField) => {
				// clone field
				const cloneNestedField = cloneDeep(nestedField);
				// delete model property
				delete cloneNestedField.model;
				// try to find nested field in revision model
				const findNestedField = revisionModel.fields.find(
					(field) => field.name === cloneNestedField.name
				);
				// if it is not present, push it in fields array
				if (findNestedField == null) {
					revisionModel.fields.push(cloneNestedField);
				}
				// and create snapshot for the nested field
				this.createDataModelSnapshot(revisionModel, cloneNestedField);
			});
		revisionModel.fields.forEach((field) => {
			// assigned by schema loader
			delete field.model;
		});
		// set revision model definition
		dataConfiguration.setModelDefinition(revisionModel);
		// append revisions attribute to parent model
		model.fields.push({
			name: 'revisions',
			type: `${name}Revision`,
			many: true,
			mapping: {
				associationType: 'association',
				cascade: 'delete',
				parentModel: name,
				parentField: 'id',
				childModel: `${name}Revision`,
				childField: 'revision_object',
			},
		});
		// ensure model listeners
		model.eventListeners = model.eventListeners || [];
		// ensure previous state listener
		if (
			model.eventListeners.find(
				(listener) => listener.type === this.PREVIOUS_STATE_LISTENER
			) == null
		) {
			model.eventListeners.unshift({
				type: this.PREVIOUS_STATE_LISTENER,
			});
		}
		// apply other listeners
		fs.readdirSync(path.resolve(__dirname, 'listeners'))
			.filter((listener) => path.extname(listener) === '.js')
			.forEach((listener) => {
				model.eventListeners.push({
					type: path.resolve(
						__dirname + '/listeners',
						path.parse(listener).name
					),
				});
			});
		// update model definition
		dataConfiguration.setModelDefinition(model);
		return revisionModel;
	}

	createDataModelSnapshot(parentModelDefinition, nestedField) {
		const configuration = this.getApplication().getConfiguration();
		const context = new LocalDataContext(configuration);
		const dataConfiguration = configuration.getStrategy(
			DataConfigurationStrategy
		);
		// set revision model definition
		dataConfiguration.setModelDefinition(parentModelDefinition);
		const parentModel = new DataModel(parentModelDefinition);
		parentModel.context = context;
		const mapping = parentModel.inferMapping(nestedField.name);
		if (mapping == null) {
			return;
		}
		let previousModel;
		if (mapping.childModel === parentModel.name) {
			previousModel = mapping.parentModel;
			mapping.parentModel += 'Snapshot';
		}
		if (mapping.parentModel === parentModel.name) {
			previousModel = mapping.childModel;
			mapping.childModel += 'Snapshot';
		}
		if (mapping.childModel === parentModel.name) {
			const mappingParentModel =
				dataConfiguration.getModelDefinition(previousModel);
			const snapshotModel = {
				name: `${mappingParentModel.name}Snapshot`,
				title: `${mappingParentModel.name}Snapshot`,
				sealed: false,
				hidden: true,
				caching: false,
				version: mappingParentModel.version,
				implements: mappingParentModel.name,
				fields: [],
				eventListeners: [],
				// preserve read privileges of parent model
				privileges: cloneDeep(mappingParentModel.privileges)
					.filter((privilege) => {
						if ((privilege.mask & 1) === 1) {
							return true;
						}
						return false;
					})
					.map((privilege) => {
						return Object.assign(privilege, { mask: 1 });
					}),
			};
			const locales = mappingParentModel.fields.find(
				(field) => field.name === 'locales'
			);
			if (locales) {
				this.createLocaleModel(snapshotModel, mappingParentModel);
				snapshotModel.fields.push({
					name: 'locales',
					type: `${snapshotModel.name}Locale`,
					mapping: {
						associationType: 'association',
						cascade: 'delete',
						parentModel: snapshotModel.name,
						parentField: 'id',
						childModel: `${snapshotModel.name}Locale`,
						childField: 'object',
					},
				});
				snapshotModel.fields.push({
					name: 'locale',
					type: `${snapshotModel.name}Locale`,
					readonly: true,
					editable: false,
					many: true,
					expandable: true,
					multiplicity: 'ZeroOrOne',
					mapping: {
						associationType: 'association',
						cascade: 'delete',
						parentModel: snapshotModel.name,
						parentField: 'id',
						childModel: `${snapshotModel.name}Locale`,
						childField: 'object',
						options: {
							$filter: 'inLanguage eq lang()',
							$first: true,
						},
					},
				});
			}
			const parentDataModel = new DataModel(mappingParentModel);
			parentDataModel.context = context;
			parentDataModel.fields
				.filter((field) => field.mapping)
				.forEach((field) => {
					const mapping = parentDataModel.inferMapping(field.name);
					if (mapping) {
						// remove field model
						delete field.model;
						const newField = cloneDeep(field);
						if (mapping.parentModel === parentDataModel.name) {
							newField.mapping.parentModel = snapshotModel.name;
						}
						if (mapping.childModel === parentDataModel.name) {
							newField.mapping.childModel = snapshotModel.name;
						}
						// do not add fields that are already added (e.g. locale,locales)
						if (snapshotModel.fields.findIndex((x=>{return x.name===field.name}))<0) {
							snapshotModel.fields.push(newField);
						}
					}
				});
			const primaryKey = parentDataModel.getAttribute(
				parentDataModel.primaryKey
			);
			if (primaryKey && primaryKey.type === 'Integer') {
				snapshotModel.fields.push({
					name: primaryKey.name,
					type: 'Counter',
					nullable: false,
					primary: true,
				});
			}
			// set model definition of snapshot model
			dataConfiguration.setModelDefinition(snapshotModel);
			delete nestedField.mapping;
			// adapt nested field type
			nestedField.type = snapshotModel.name;
			return snapshotModel;
		} else if (
			mapping.parentModel === parentModel.name &&
			mapping.associationType !== 'junction'
		) {
			const childModel = dataConfiguration.getModelDefinition(previousModel);
			const snapshotModel = {
				name: `${childModel.name}Snapshot`,
				title: `${childModel.name}Snapshot`,
				sealed: false,
				hidden: true,
				caching: false,
				version: childModel.version,
				implements: childModel.name,
				fields: [],
				eventListeners: [],
				// preserve read privileges of parent model
				privileges: cloneDeep(childModel.privileges)
					.filter((privilege) => {
						if ((privilege.mask & 1) === 1) {
							return true;
						}
						return false;
					})
					.map((privilege) => {
						return Object.assign(privilege, { mask: 1 });
					}),
			};
			const field = childModel.fields.find(
				(field) => field.type === parentModel.implements
			);
			field.type = parentModel.name;
			const parentField = parentModel.fields.find(
				(field) => field.type === previousModel
			);
			if (parentField && parentField.mapping) {
				parentField.mapping.parentModel = parentModel.name;
				parentField.mapping.childModel = snapshotModel.name;
				parentField.type = snapshotModel.name;
			}
			delete field.model;
			// push field to snapshot model
			snapshotModel.fields.push(field);
			// field.mapping.parentModel = parentModel.name;
			// field.mapping.childModel = snapshotModel.name;
			const locales = childModel.fields.find(
				(field) => field.name === 'locales'
			);
			if (locales) {
				this.createLocaleModel(snapshotModel, childModel);
				snapshotModel.fields.push({
					name: 'locales',
					type: `${snapshotModel.name}Locale`,
					mapping: {
						associationType: 'association',
						cascade: 'delete',
						parentModel: snapshotModel.name,
						parentField: 'id',
						childModel: `${snapshotModel.name}Locale`,
						childField: 'object',
					},
				});
				snapshotModel.fields.push({
					name: 'locale',
					type: `${snapshotModel.name}Locale`,
					readonly: true,
					editable: false,
					many: true,
					expandable: true,
					multiplicity: 'ZeroOrOne',
					mapping: {
						associationType: 'association',
						cascade: 'delete',
						parentModel: snapshotModel.name,
						parentField: 'id',
						childModel: `${snapshotModel.name}Locale`,
						childField: 'object',
						options: {
							$filter: 'inLanguage eq lang()',
							$first: true,
						},
					},
				});
			}
			// set model definition of snapshot model
			dataConfiguration.setModelDefinition(snapshotModel);
			delete nestedField.mapping;
			// adapt nested field type
			nestedField.type = snapshotModel.name;
		} else {
			throw new Error('The specified association type is not yet implemented.');
		}
	}

	createLocaleModel(model, sourceModel) {
		// get data configuration
		const dataConfiguration = this.getApplication()
			.getConfiguration()
			.getStrategy(DataConfigurationStrategy);
		// try to find locales property of source model
		const locales = sourceModel.fields.find(
			(field) => field.name === 'locales'
		);
		// if it is null, return
		if (locales == null) {
			return;
		}
		// const localeModel = dataConfiguration.getModelDefinition(locales.type);
		// get source locale model before changing type
		const sourceLocaleModel = dataConfiguration.getModelDefinition(
			locales.type
		);
		// add localization listener to (parent) model
		model.eventListeners = [
			{
				type: this.DATA_LOCALIZATION_LISTENER,
			},
		];

		// create new locale model
		// important note: Handle id, inLanguage and object fields.
		const newLocaleModel = {
			name: `${model.name}Locale`,
			title: `${model.name}Locale`,
			sealed: false,
			hidden: true,
			caching: false,
			version: sourceLocaleModel.version,
			implements: sourceLocaleModel.name,
			fields: [
				{
					name: 'id',
					type: 'Guid',
					nullable: false,
					primary: true,
					value: 'javascript:return this.newGuid();',
				},
				{
					name: 'object',
					title: 'object',
					description: 'The parent item.',
					type: model.name,
				},
				{
					name: 'inLanguage',
					title: 'inLanguage',
					description: 'The language of this item.',
					type: 'Text',
					size: 5,
				},
			],
			eventListeners: [],
			// preserve read privileges of parent model
			privileges: cloneDeep(sourceLocaleModel.privileges)
				.filter((privilege) => {
					if ((privilege.mask & 1) === 1) {
						return true;
					}
					return false;
				})
				.map((privilege) => {
					return Object.assign(privilege, { mask: 1 });
				}),
			constraints: [
				{
					type: 'unique',
					fields: ['object', 'inLanguage'],
				},
			],
		};

		// set model definition
		dataConfiguration.setModelDefinition(newLocaleModel);
		return newLocaleModel;
	}
}
