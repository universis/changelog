import { DataObjectState } from '@themost/data';
import { DataError, DataNotFoundError } from '@themost/common';
const semver = require('semver');

export function afterSave(event, callback) {
	if (event.model.versioning && event.model.versioning.ignoreUpdate === true) {
		if (!(event.options && event.options.forceUpdateListener)) {
			return callback();
		}
	}
	(async () => {
		if (event.state === DataObjectState.Update) {
			const context = event.model.context;
			const previous = event.previous;
			const target = event.target;
			if (previous == null) {
				throw new DataError(
					'E_STATE',
					'The previous state of the object cannot be determined',
					null,
					event.model.name
				);
			}
			// get last revision of the item
			const lastRevision = await context
				.model(`${event.model.name}Revision`)
				.where('revision_object')
				.equal(previous[event.model.primaryKey])
				.orderByDescending('revision_version')
				.select('id', 'revision_version')
				.getItem();
			if (lastRevision == null) {
				throw new DataNotFoundError(
					'The last revision of the object cannot be found or is inaccessible. It cannot be empty at this context.'
				);
			}
			const localesAttribute = event.model.getAttribute('locales');
			let fullTarget;
			if (localesAttribute) {
				// populate target data
				fullTarget = await context
					.model(event.model.name)
					.where(event.model.primaryKey)
					.equal(target[event.model.primaryKey])
					.flatten()
					.expand('locales')
					.getItem();
				if (fullTarget.locales && fullTarget.locales.length) {
					for (const locale of fullTarget.locales) {
						// delete id and object properties
						delete locale.id;
						delete locale.object;
					}
				}
			} else {
				fullTarget = await context
					.model(event.model.name)
					.where(event.model.primaryKey)
					.equal(target[event.model.primaryKey])
					.getItem();
			}
			delete fullTarget[event.model.primaryKey];
			// assign extra attributes
			Object.assign(fullTarget, {
				revision_object: target[event.model.primaryKey],
				revision_version: semver.inc(lastRevision.revision_version, 'minor'),
				revision_tag: 'latest',
			});
			event.model.fields
				.filter((field) => field.nested)
				.forEach((field) => {
					if (Object.prototype.hasOwnProperty.call(fullTarget, field.name)) {
						const nestedModel = context.model(field.type);
						if (nestedModel) {
							const value = fullTarget[field.name];
							const primaryKey = nestedModel.primaryKey;
							if (Array.isArray(value)) {
								value.forEach((item) => {
									delete item[primaryKey];
								});
							} else {
								delete value[primaryKey];
							}
						}
					}
				});
			// save next revision
			await context
				.model(`${event.model.name}Revision`)
				.silent()
				.save(fullTarget);
			// update previous revision's tag (null)
			Object.assign(lastRevision, {
				revision_tag: null,
				$state: 2,
			});
			await context
				.model(`${event.model.name}Revision`)
				.silent()
				.save(lastRevision);
		}
	})()
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}
