import { DataObjectState } from '@themost/data';
import { DataError } from '@themost/common';
import { cloneDeep } from 'lodash';

export function afterSave(event, callback) {
	if (event.model.versioning && event.model.versioning.ignoreUpdate === true) {
		if (!(event.options && event.options.forceUpdateListener)) {
			return callback();
		}
	}
	(async () => {
		// on insert
		if (event.state === DataObjectState.Insert) {
			const context = event.model.context;
			const target = event.target;
			// try to fetch locales property of model
			const localesAttribute = event.model.getAttribute('locales');
			let fullTarget;
			if (localesAttribute) {
				// get full target data
				fullTarget = await context
					.model(event.model.name)
					.where(event.model.primaryKey)
					.equal(event.target[event.model.primaryKey])
					.flatten()
					.expand('locales')
					.getItem();
				if (fullTarget.locales && fullTarget.locales.length) {
					for (const locale of fullTarget.locales) {
						// delete locale id and object
						delete locale.id;
						delete locale.object;
					}
				}
			} else {
				fullTarget = await context
					.model(event.model.name)
					.where(event.model.primaryKey)
					.equal(event.target[event.model.primaryKey])
					.getItem();
			}
			// delete primary key
			delete fullTarget[event.model.primaryKey];
			// assign extra attributes
			Object.assign(fullTarget, {
				revision_object: target[event.model.primaryKey],
				revision_version: '1.0.0',
				revision_tag: 'latest',
			});
			event.model.fields
				.filter((field) => field.nested)
				.forEach((field) => {
					if (Object.prototype.hasOwnProperty.call(fullTarget, field.name)) {
						const nestedModel = context.model(field.type);
						if (nestedModel) {
							const value = fullTarget[field.name];
							const primaryKey = nestedModel.primaryKey;
							if (Array.isArray(value)) {
								value.forEach((item) => {
									delete item[primaryKey];
								});
							} else {
								delete value[primaryKey];
							}
						}
					}
				});
			// create revision
			await context
				.model(`${event.model.name}Revision`)
				.silent()
				.save(fullTarget);
		}
	})()
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

export function beforeSave(event, callback) {
	// check if update is enabled
	if ((event.model.versioning && event.model.versioning.ignoreUpdate === true)) {
		if (!(event.options && event.options.forceUpdateListener)) {
			return callback();
		}
	}
	(async () => {
		// on update
		if (event.state === DataObjectState.Update) {
			const context = event.model.context;
			const previous = event.previous;

			if (previous == null) {
				throw new DataError(
					'E_STATE',
					'The previous state of the object cannot be determined',
					null,
					event.model.name
				);
			}
			// check if a revision exists for this object
			const revisionExists = await context
				.model(`${event.model.name}Revision`)
				.where('revision_object')
				.equal(previous[event.model.primaryKey])
				.silent()
				.count();
			// if it does, just return
			if (revisionExists) {
				return;
			}
			let revisionState;
			const localesAttribute = event.model.getAttribute('locales');
			if (localesAttribute) {
				revisionState = await context
					.model(event.model.name)
					.where(event.model.primaryKey)
					.equal(previous[event.model.primaryKey])
					.flatten()
					.expand('locales')
					.getItem();
				if (revisionState.locales && revisionState.locales.length) {
					for (const locale of revisionState.locales) {
						// delete locale id and object
						delete locale.id;
						delete locale.object;
					}
				}
			} else {
				revisionState = cloneDeep(previous);
			}
			delete revisionState[event.model.primaryKey];
			// assign extra attributes
			Object.assign(revisionState, {
				revision_object: previous[event.model.primaryKey],
				revision_version: '1.0.0',
				revision_tag: 'latest',
			});
			event.model.fields
				.filter((field) => field.nested)
				.forEach((field) => {
					if (Object.prototype.hasOwnProperty.call(revisionState, field.name)) {
						const nestedModel = context.model(field.type);
						if (nestedModel) {
							const value = revisionState[field.name];
							const primaryKey = nestedModel.primaryKey;
							if (Array.isArray(value)) {
								value.forEach((item) => {
									delete item[primaryKey];
								});
							} else {
								delete value[primaryKey];
							}
						}
					}
				});
			// create revision
			await context
				.model(`${event.model.name}Revision`)
				.silent()
				.save(revisionState);
		}
	})()
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}
