import { DataModel } from '@themost/data';
import { beforeSave } from './set-initial-object';
import { DataNotFoundError } from '@themost/common';

/**
 * @param {DataEventArgs} event
 */
async function beforeRemoveAsync(event) {
	const model = event.model;
	if (model.versioning && model.versioning.ignoreDelete === true) {
		return;
	}
	// set ignoreUpdate to false, so update listeners can add deleted object
	event.options = event.options || {};
	event.options.forceUpdateListener = true;
	const getReferenceMappings = DataModel.prototype.getReferenceMappings;
	model.getReferenceMappings = async function () {
		const res = await getReferenceMappings.bind(this)();
		// remove revision mappings before deletion
		const mappings = [`${model.name}Revision`];
		if ( event.options.excludeMappings) {
			mappings.push.apply(mappings, event.options.excludeMappings);
		}
		return res.filter((mapping) => {
			return mappings.indexOf(mapping.childModel) < 0;
		});
	};
	await new Promise((resolve, reject) => {
		void beforeSave(
			{
				state: 2,
				target: event.previous,
				previous: event.previous,
				model: event.model,
				options : event.options
			},
			function (err) {
				if (err) {
					return reject(err);
				}
				return resolve();
			}
		);
	});
}

/**
 * @param {DataEventArgs} event
 */
async function afterRemoveAsync(event) {
	if (event.model.versioning && event.model.versioning.ignoreDelete === true) {
		return;
	}
	const previous = event.previous;
	const context = event.model.context;
	// get last revision of the item
	const lastRevision = await context
		.model(`${event.model.name}Revision`)
		.where('revision_object')
		.equal(previous[event.model.primaryKey])
		.orderByDescending('revision_version')
		.select('id', 'revision_version')
		.getItem();
	if (lastRevision == null) {
		throw new DataNotFoundError(
			'The last revision of the object cannot be found or is inaccessible. It cannot be empty at this context.'
		);
	}
	const updateRevision = {
		id: lastRevision.id,
		revision_tag: 'deleted',
	};
	// and update
	await context
		.model(`${event.model.name}Revision`)
		.silent()
		.save(updateRevision);
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeRemove(event, callback) {
	return beforeRemoveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterRemove(event, callback) {
	return afterRemoveAsync(event)
		.then(() => {
			return callback();
		})
		.catch((err) => {
			return callback(err);
		});
}
