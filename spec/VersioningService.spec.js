import { VersioningService } from "../src/VersioningService";
import app from "../../../dist/server/app";
import { ExpressDataApplication, ExpressDataContext } from "@themost/express";
import { DataConfigurationStrategy } from "@themost/data";
import { TestUtils } from "../../../dist/server/utils";
const executeInTransaction = TestUtils.executeInTransaction;
const mockPerson = {
	familyName: "familyName",
	givenName: "givenName",
	gender: {
		alternateName: "male",
	},
	fatherName: "fatherName",
	motherName: "motherName",
	email: "email@example.com",
};

const mockInternship = {
	internshipYear: 2021,
	name: "Test internship",
	status: {
		alternateName: "active",
	},
};

describe("VersioningService", () => {
	let application;
	let context;

	beforeAll((done) => {
		application = app.get(ExpressDataApplication.name);
		return done();
	});

	beforeEach(async () => {
		context = application.createContext();
	});

	afterEach((done) => {
		// important: clear cache after each test
		const configuration = context.getConfiguration();
		if (configuration.hasOwnProperty("cache")) {
			delete configuration.cache;
		}
		context.finalize(() => {
			return done();
		});
	});
	it("should implement versioning for Person model", async () => {
		await executeInTransaction(context, async () => {
			expect(context).toBeInstanceOf(ExpressDataContext);

			// get data configuration
			const dataConfiguration = application
				.getConfiguration()
				.getStrategy(DataConfigurationStrategy);
			// get Person model definition
			let model = dataConfiguration.getModelDefinition("Person");
			expect(model).toBeTruthy();
			// apply versioning attribute
			Object.assign(model, {
				versioning: true,
			});
			// update model definition
			dataConfiguration.setModelDefinition(model);
			// refetch model and validate that revisions do not-yet-exist
			model = dataConfiguration.getModelDefinition("Person");
			expect(model.versioning).toBeTruthy();
			expect(
				dataConfiguration.getModelDefinition("PersonRevision")
			).toBeFalsy();
			expect(
				dataConfiguration.getModelDefinition("PersonRevisionLocale")
			).toBeFalsy();
			// install service
			new VersioningService(application);
			// now that service is installed
			// validate PersonRevision model
			const personRevision =
				dataConfiguration.getModelDefinition("PersonRevision");
			// validate PersonRevisionLocale model
			const PersonRevisionLocale = dataConfiguration.getModelDefinition(
				"PersonRevisionLocale"
			);
			// validate that Person model locale mappings are not changed
			model = dataConfiguration.getModelDefinition("Person");
			expect(model.fields.find((field) => field.name === "locales").type).toBe(
				"PersonLocale"
			);
			expect(model.fields.find((field) => field.name === "locale").type).toBe(
				"PersonLocale"
			);
			// validate some priviliges
			expect(PersonRevisionLocale).toBeTruthy();
			expect(personRevision).toBeTruthy();
			expect(personRevision.privileges.length).toBeGreaterThan(0);
			expect(personRevision.privileges[0].mask).toBe(1);
			expect(PersonRevisionLocale.privileges.length).toBeGreaterThan(0);
			expect(PersonRevisionLocale.privileges[0].mask).toBe(1);

			// validate PersonRevisionLocale fields
			expect(
				PersonRevisionLocale.fields.find((field) => field.name === "object")
					.type
			).toBe("PersonRevision");
			expect(
				PersonRevisionLocale.fields.find((field) => field.name === "id").type
			).toBe("Guid");
			expect(
				PersonRevisionLocale.fields.find((field) => field.name === "inLanguage")
					.readonly
			).toBeFalsy();
			// validate PersonRevision locales and locale property
			const localesProperty = personRevision.fields.find(
				(field) => field.name === "locales"
			);
			expect(localesProperty).toBeTruthy();
			expect(localesProperty.type).toBe("PersonRevisionLocale");
			const localeProperty = personRevision.fields.find(
				(field) => field.name === "locale"
			);
			expect(localeProperty).toBeTruthy();
			expect(localeProperty.type).toBe("PersonRevisionLocale");

			// mock an admin user. Also tests the read permission preservance on the revision model (PersonRevision in this case).
			// do not use silent any more, after the call below
			const user = await context
				.model("User")
				.where("groups/name")
				.equal("Administrators")
				.silent()
				.first();
			expect(user).toBeTruthy(
				"Cannot find any administrator users in the database."
			);
			context.user = {
				name: user.name,
			};
			// create a new person
			const result = await context.model("Person").silent().save(mockPerson);
			expect(result).toBeTruthy();
			// fetch first revision (1.0.0)
			const firstRevision = await context
				.model("Person")
				.where("id")
				.equal(result.id)
				.flatten()
				.expand(
					{
						name: "revisions",
						options: {
							$expand: "locales",
						},
					},
					"locales"
				)
				.getItem();
			/* --- validate first revision attributes --- */
			expect(firstRevision.revisions.length).toBeGreaterThan(0);
			expect(firstRevision.revisions[0].email).toEqual(mockPerson.email);
			expect(firstRevision.revisions[0].givenName).toEqual(
				mockPerson.givenName
			);
			expect(firstRevision.revisions[0].familyName).toEqual(
				mockPerson.familyName
			);
			expect(firstRevision.revisions[0].fatherName).toEqual(
				mockPerson.fatherName
			);
			expect(firstRevision.revisions[0].motherName).toEqual(
				mockPerson.motherName
			);
			expect(firstRevision.revisions[0].revision_version).toEqual("1.0.0");
			expect(firstRevision.revisions[0].revision_tag).toEqual("latest");
			expect(firstRevision.revisions[0].locales.length).toBeGreaterThan(0);

			// refetch the new person and update it
			await context
				.model("Person")
				.silent()
				.save(Object.assign(mockPerson, { familyName: "newFamilyName" }));
			// fetch revisions (now must be 1.0.0 and 1.1.0)
			const secondRevision = await context
				.model("Person")
				.where("id")
				.equal(result.id)
				.flatten()
				.expand({
					name: "revisions",
					options: {
						$expand: "locales",
					},
				})
				.getItem();
			expect(secondRevision.revisions.length).toEqual(2); // two revisions
			// validate first revision existance
			expect(
				secondRevision.revisions.find(
					(revision) => revision.revision_version === "1.0.0"
				)
			).toBeTruthy();
			// validate second revision
			const rev1_1_0 = secondRevision.revisions.find(
				(revision) => revision.revision_version === "1.1.0"
			);
			expect(rev1_1_0).toBeTruthy();
			expect(rev1_1_0.familyName).toBe("newFamilyName");
			// get a person that already exists and has no revisions
			const alreadyExistingPerson = await context
				.model("Person")
				.asQueryable()
				.where("revisions/id")
				.equal(null)
				.first();
			expect(alreadyExistingPerson).toBeTruthy(
				"Cannot find a person record without revisions in the database"
			);
			// keep familyName before update
			const familyNameBeforeUpdate = alreadyExistingPerson.familyName;
			// avoid unwanted db errors on birthdate
			alreadyExistingPerson.birthDate = null;
			// update person
			await context.model("Person").save(
				Object.assign(alreadyExistingPerson, {
					familyName: "newFamilyName",
				})
			);
			// fetch person again and validate revisions.
			// two of them must be present, 1.0.0 and 1.1.0.
			const updateRevisions = await context
				.model("Person")
				.where("id")
				.equal(alreadyExistingPerson.id)
				.flatten()
				.expand({
					name: "revisions",
					options: {
						$expand: "locales, revision_creator",
					},
				})
				.getItem();
			const rev1 = updateRevisions.revisions.find(
				(revision) => revision.revision_version === "1.0.0"
			);
			const rev2 = updateRevisions.revisions.find(
				(revision) => revision.revision_version === "1.1.0"
			);
			expect(rev1).toBeTruthy();
			expect(rev2).toBeTruthy();
			// validate revisions 1.0.0	and 1.1.0 and some key attributes
			expect(updateRevisions.revisions.length).toBe(2);
			expect(rev1.familyName).toBe(familyNameBeforeUpdate); // validate family name before update
			expect(rev2.familyName).toBe("newFamilyName"); // validate family name after update
			expect(rev1.revision_tag).toBeFalsy();
			expect(rev2.revision_tag).toBe("latest");
			// validate revision_creator
			expect(rev1.revision_creator.name).toBe(user.name);
			expect(rev2.revision_creator.name).toBe(user.name);
		});
	});

	it("should implement versioning for Student model", async () => {
		await executeInTransaction(context, async () => {
			expect(context).toBeInstanceOf(ExpressDataContext);
			// get data configuration
			const dataConfiguration = application
				.getConfiguration()
				.getStrategy(DataConfigurationStrategy);
			// get Person model definition
			let model = dataConfiguration.getModelDefinition("Student");
			expect(model).toBeTruthy();
			// apply versioning attribute
			Object.assign(model, {
				versioning: true,
			});
			// update model definition
			dataConfiguration.setModelDefinition(model);
			// refetch model and validate that revisions do not-yet-exist
			model = dataConfiguration.getModelDefinition("Student");
			expect(model.versioning).toBeTruthy();
			expect(
				dataConfiguration.getModelDefinition("StudentRevision")
			).toBeFalsy();
			// install service
			new VersioningService(application);
			// now that service is installed
			// validate StudentRevision model
			const studentRevision =
				dataConfiguration.getModelDefinition("StudentRevision");
			// validate some priviliges
			expect(studentRevision).toBeTruthy();
			expect(studentRevision.privileges.length).toBeGreaterThan(0);
			expect(studentRevision.privileges[0].mask).toBe(1);
			model = dataConfiguration.getModelDefinition("PersonSnapshot");
			expect(model).toBeTruthy();
			// validate PersonSnapshot locale and locales properties
			const localesProperty = model.fields.find(
				(field) => field.name === "locales"
			);
			expect(localesProperty).toBeTruthy();
			expect(localesProperty.type).toBe("PersonSnapshotLocale");
			const localeProperty = model.fields.find(
				(field) => field.name === "locale"
			);
			expect(localeProperty).toBeTruthy();
			expect(localeProperty.type).toBe("PersonSnapshotLocale");
			model = dataConfiguration.getModelDefinition("PersonSnapshotLocale");
			expect(model).toBeTruthy();
			// validate PersonSnapshotLocale fields
			expect(model.fields.find((field) => field.name === "object").type).toBe(
				"PersonSnapshot"
			);
			expect(model.fields.find((field) => field.name === "id").type).toBe(
				"Guid"
			);
			expect(
				model.fields.find((field) => field.name === "inLanguage").readonly
			).toBeFalsy();
		});
	});

	it("should implement versioning for Institute model", async () => {
		await executeInTransaction(context, async () => {
			expect(context).toBeInstanceOf(ExpressDataContext);
			// get data configuration
			const dataConfiguration = application
				.getConfiguration()
				.getStrategy(DataConfigurationStrategy);
			// get Person model definition
			let model = dataConfiguration.getModelDefinition("Institute");
			expect(model).toBeTruthy();
			// apply versioning attribute
			Object.assign(model, {
				versioning: true,
			});
			// update model definition
			dataConfiguration.setModelDefinition(model);
			// refetch model and validate that revisions do not-yet-exist
			model = dataConfiguration.getModelDefinition("Institute");
			expect(model.versioning).toBeTruthy();
			expect(
				dataConfiguration.getModelDefinition("InstituteRevision")
			).toBeFalsy();
			// install service
			new VersioningService(application);
			// now that service is installed
			// validate InstituteRevision model
			const instituteRevision =
				dataConfiguration.getModelDefinition("InstituteRevision");
			// validate some priviliges
			expect(instituteRevision).toBeTruthy();
			expect(instituteRevision.privileges.length).toBeGreaterThan(0);
			expect(instituteRevision.privileges[0].mask).toBe(1);
			// validate institute configuration type of revision model
			expect(
				instituteRevision.fields.find(
					(field) => field.name === "instituteConfiguration"
				).type
			).toBe("InstituteConfigurationSnapshot");
			model = dataConfiguration.getModelDefinition(
				"InstituteConfigurationSnapshot"
			);
			expect(model).toBeTruthy();
			// validate institute field of configuration snapshot
			expect(
				model.fields.find((field) => field.name === "institute").type
			).toBe("InstituteRevision");
		});
	});

	it("should implement versioning for StudyProgram model", async () => {
		await executeInTransaction(context, async () => {
			expect(context).toBeInstanceOf(ExpressDataContext);
			// get data configuration
			const dataConfiguration = application
				.getConfiguration()
				.getStrategy(DataConfigurationStrategy);
			// get Person model definition
			let model = dataConfiguration.getModelDefinition("StudyProgram");
			expect(model).toBeTruthy();
			// apply versioning attribute
			Object.assign(model, {
				versioning: true,
			});
			// update model definition
			dataConfiguration.setModelDefinition(model);
			// refetch model and validate that revisions do not-yet-exist
			model = dataConfiguration.getModelDefinition("StudyProgram");
			expect(model.versioning).toBeTruthy();
			expect(
				dataConfiguration.getModelDefinition("StudyProgramRevision")
			).toBeFalsy();
			// install service
			new VersioningService(application);
			// now that service is installed
			// validate PersonRevision model
			const studyProgramRevision = dataConfiguration.getModelDefinition(
				"StudyProgramRevision"
			);
			// validate some priviliges
			expect(studyProgramRevision).toBeTruthy();
			expect(studyProgramRevision.privileges.length).toBeGreaterThan(0);
			expect(studyProgramRevision.privileges[0].mask).toBe(1);
			model = dataConfiguration.getModelDefinition(
				"StudyProgramExtensionSnapshot"
			);
			expect(model).toBeTruthy();
			// validate PersonSnapshot locale and locales properties
			const localesProperty = model.fields.find(
				(field) => field.name === "locales"
			);
			expect(localesProperty).toBeTruthy();
			expect(localesProperty.type).toBe("StudyProgramExtensionSnapshotLocale");
			const localeProperty = model.fields.find(
				(field) => field.name === "locale"
			);
			expect(localeProperty).toBeTruthy();
			expect(localeProperty.type).toBe("StudyProgramExtensionSnapshotLocale");
			model = dataConfiguration.getModelDefinition(
				"StudyProgramExtensionSnapshotLocale"
			);
			expect(model).toBeTruthy();
			// validate StudyProgramExtensionSnapshotLocale fields
			expect(model.fields.find((field) => field.name === "object").type).toBe(
				"StudyProgramExtensionSnapshot"
			);
			expect(model.fields.find((field) => field.name === "id").type).toBe(
				"Guid"
			);
			expect(
				model.fields.find((field) => field.name === "inLanguage").readonly
			).toBeFalsy();
			// validate that Person model locale mappings are not changed
			model = dataConfiguration.getModelDefinition("StudyProgramExtension");
			expect(model.fields.find((field) => field.name === "locales").type).toBe(
				"StudyProgramExtensionLocale"
			);
			expect(model.fields.find((field) => field.name === "locale").type).toBe(
				"StudyProgramExtensionLocale"
			);
		});
	});

	fit("should implement versioning for Internship model and allow deletion", async () => {
		await executeInTransaction(context, async () => {
			expect(context).toBeInstanceOf(ExpressDataContext);

			// get data configuration
			const dataConfiguration = application
				.getConfiguration()
				.getStrategy(DataConfigurationStrategy);
			// get Internship model definition
			let model = dataConfiguration.getModelDefinition("Internship");
			expect(model).toBeTruthy();
			// apply versioning attribute
			Object.assign(model, {
				versioning: true,
			});
			// update model definition
			dataConfiguration.setModelDefinition(model);
			// refetch model and validate that revisions do not-yet-exist
			model = dataConfiguration.getModelDefinition("Internship");
			expect(model.versioning).toBeTruthy();
			expect(
				dataConfiguration.getModelDefinition("InternshipRevision")
			).toBeFalsy();
			// install service
			new VersioningService(application);
			// now that service is installed
			// validate PersonRevision model
			const internshipRevision =
				dataConfiguration.getModelDefinition("InternshipRevision");

			// validate some priviliges
			expect(internshipRevision).toBeTruthy();
			expect(internshipRevision.privileges.length).toBeGreaterThan(0);
			expect(internshipRevision.privileges[0].mask).toBe(1);

			// mock an admin user. Also tests the read permission preservance on the revision model (PersonRevision in this case).
			// do not use silent any more, after the call below
			const user = await context
				.model("User")
				.where("groups/name")
				.equal("Administrators")
				.silent()
				.first();
			expect(user).toBeTruthy(
				"Cannot find any administrator users in the database."
			);
			context.user = {
				name: user.name,
			};
			// get a department
			const department = await context
				.model("Department")
				.asQueryable()
				.first();
			expect(department).toBeTruthy();
			// assign department to internship
			mockInternship.department = department.id;
			// create a new Internship
			const result = await context
				.model("Internship")
				.silent()
				.save(mockInternship);
			expect(result).toBeTruthy();
			// fetch first revision (1.0.0)
			const firstRevision = await context
				.model("Internship")
				.where("id")
				.equal(result.id)
				.flatten()
				.expand("revisions")
				.getItem();
			/* --- validate first revision attributes --- */
			expect(firstRevision.revisions.length).toBeGreaterThan(0);
			expect(firstRevision.revisions[0].name).toEqual(mockInternship.name);
			expect(firstRevision.revisions[0].revision_version).toEqual("1.0.0");
			expect(firstRevision.revisions[0].revision_tag).toEqual("latest");

			// refetch the new Internship and update it
			await context
				.model("Internship")
				.silent()
				.save(Object.assign(mockInternship, { name: "Test new name" }));
			// fetch revisions (now must be 1.0.0 and 1.1.0)
			const secondRevision = await context
				.model("Internship")
				.where("id")
				.equal(result.id)
				.flatten()
				.expand("revisions")
				.getItem();
			expect(secondRevision.revisions.length).toEqual(2); // two revisions
			// validate first revision existance
			expect(
				secondRevision.revisions.find(
					(revision) => revision.revision_version === "1.0.0"
				)
			).toBeTruthy();
			// validate second revision
			const rev1_1_0 = secondRevision.revisions.find(
				(revision) => revision.revision_version === "1.1.0"
			);
			expect(rev1_1_0).toBeTruthy();
			expect(rev1_1_0.name).toBe("Test new name");
			expect(rev1_1_0.revision_tag).toBe("latest");
			// expect successful removal
            await expectAsync((function() {
                return context.model('Internship').silent().save({
					id: result.id,
					$state: 4
				});
            })()).toBeResolved();
			// expect to still find revisions
			const previousRevisions = await context.model('InternshipRevision')
				.asQueryable()
				.silent()
				.getAllItems();
			expect(previousRevisions).toBeTruthy();
			expect(previousRevisions.length).toBe(2);
			const deletedInternship = await context.model('Internship')
				.where('id').equal(previousRevisions[0].revision_object)
				.silent()
				.getItem();
			expect(deletedInternship).toBeFalsy();
		});
	});
});
