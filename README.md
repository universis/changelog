# @universis/changelog

Universis api server versioning service

`@universis/changelog` provides a low-level infrastructure for versioning data of any kind.

## Usage

    npm i @universis/changelog

Register `@universis/changelog#VersioningService` under services section of application configuration:

    {
        "services": {
            ...,
            {
                "serviceType": "@universis/changelog#VersioningService"
            }
        }
    }

Every data model may have `versioning` attribute which indicates whether a new version of data will be stored after any CRUD operation or not.

    {
        "name": "Student",
        ...,
        "versioning": true
    }

e.g. `Student` model has `versioning` enabled. This operation will include automatically `StudentRevision` model which is intended to be used for storing student record revisions and `PersonSnapshot` which is a snapshot of `Person` model -`Person` has an one-to-one association with `Student`-.

